---
title: "The Strategy Pattern - Design Pattern Series"
---

<p>A strategy is an action you can take to achieve a specific outcome. Depending on your circumstances, you might want to try out one action over another.</p>

<p>If you happen to be a cat named Leo (DeCatprio) and your outcome is to get attention, the strategies you could use are:</p>

<ul>
  <li>Meow loudly and repeatedly</li>
  <li>Sit on hoomans keyboard while they're trying to type</li>
  <li>Knock things off the table</li>
  <li>Eat earplugs and cough loudly</li>
</ul>

<p>Which strategy you use at any given point can be decided by you, or something else can decide for you. For example, you might decide to get attention when you realise that you're hungry, or your hooman might ask you to get attention so they can make you a star on YouTube. What's important is that you've been trained to respond to the message "get attention".</p>

<p>When you hear the message "get attention", you immediately delegate that behavior to the only strategy you know about (the one you were given by yourself or someone else, or default behaviour if none was given).</p>

<p>You ask whatever strategy you know about to go and "get attention" on your behalf. The strategy then goes and implements that behaviour and passes the result back to you, which you then pass back to yourself or the person that asked you to get attention in the first place.</p>

<p>The person (or stomach) that asked you to get attention has no idea how you got it, and they don't care because they have the result of that behaviour which is all they cared about.</p>

<p>You the kitty (not your stomach) also have no idea how you got attention, because you delegated it. You're also pretty happy about this because anytime a new strategy for getting attention is discovered, you'll be able to use it straight away without ever having to remember how to do all of them at once.</p>

<pre>
<code>
// Strategy Interface - Says: "All strategies will have a seek method that returns a string value"
interface Attention {
  seek(): string
}

// Concrete Strategy - The implementation of a specific strategy. Make one for each strategy
class MeowLoudlyAndRepeatedly implements Attention {
  seek(): string { return "MEOW! MEOW! MEOW! MEOW!" }
}

class KnockThingsOffTheTable implements Attention {
  seek(): string { return "*Knocks one item off the table while looking straight at you*" }
}

// Context Object - Has a concrete strategy (or default behaviour) that can be switched out and used
class Kitty {
  attention: Attention = new MeowLoudlyAndRepeatedly

  getAttentionBy(strategy: Attention): void { this.attention = strategy }
  getAttention(): string { return this.attention.seek() }
}

// Using the above in practice - So easy to switch out strategies
const leoDecatprio = new Kitty
leoDecatprio.getAttention()
leoDecatprio.getAttentionBy(new KnockThingsOffTheTable)
leoDecatprio.getAttention()
</code>
</pre>

<p>Sometimes, the strategy needs data from the context object (or vise versa). There are two ways to do this mentioned in the design pattern book.</p>

<ol>
  <li>The context object (kitty) passes it's own data (hunger status) to the strategy method (getAttention()).</li>
  <li>The context object passes itself (this) to the strategy method (getAttention()).</li>
</ol>

<p>Which you choose depend on whether different strategies care about different information (pass the whole context), or whether they all only care about one or two things (pass the data).</p>

<h2>Clients and default behaviour</h2>

<ul>
  <li>Clients usually create and pass a ConcreteStrategy object to the context (kitty). The client then interacts exclusively with the context)</li>
  <li>Clients must be aware of different strategies, and understand how they differ before they can choose the appropriate one.</li>
  <li>Default behaviour (optional) -> Context can check to see if it has a Strategy object before implementing it. If there isn't one, then context can carry out default behaviour.</li>
</ul>

<h2>Extra Tip</h2>

<p>Name methods from the point of view of the caller.</p>
