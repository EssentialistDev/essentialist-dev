![Full home page screen capture](/assets/images/logo-full.png)

# Essentialist Dev | A Live Developer Journal

**Tags:** Blog, Website, Jekyll, Material Design, Katas, Wellbeing, Tech, Portfolio, Newsletter

## Essentialism

The name "Essentialist Dev" is inspired by Greg McKeown's Book [Essentialism: The Disciplined Pursuit of Less](https://gregmckeown.com/book/).

It's easy to get overwhelmed by how much there is to learn as a developer. This book helped me realise that we can choose to do anything, but not everything.

> Only once you stop giving yourself permission to do it all, to stop saying yes to everyone, can you make your highest contribution to the things that really matter. - Greg McKeown